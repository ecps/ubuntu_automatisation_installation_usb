# Ubuntu automated usb installation

## Prerequis

Prepare a Usb stick with rufus with al least 10GB presistent storage

### Create an image of basic ubuntu with usb live
Run the Ubuntu usb live and install a basic Ubuntu system on the hard drive with parameter below later on we will make an image of the current installation you need 
```
/boot as XFS and size of 300MB
/ (root) as XFS and size of 60GB
```
After the installation if done make an image of it, **boot again with ubuntu usb live stick** then proceed with command below

```
apt-add-repository universe
apt install -y partclone git

#Replace the /dev/sda? with your label
partclone.xfs -c -s /dev/sda1 -o p2_boot
partclone.xfs -c -s /dev/sda4 -o p4_root
```
Compress the image root and split the image in multiple partition
```
gzip p4_root
split -b 536870912 p4_root.gz p4_root.gz.part
```
set up the network configuration file as needed for your setup in ubuntu_automatisation_installation_usb/templates/01-network-manager-all.yaml. Find the current configuration below
```
#change the network interface as needed
network:
    ethernets:
        ens33:
            dhcp4: false
            addresses: [{1}/24]
            gateway4: {2}
            nameservers:
              addresses: [8.8.8.8, 1.1.1.1]
    version: 2

```
**git clone this project and copy the image p2_boot and p4_root.gz.part into ubuntu_automatisation_installation_usb/image/**
## installation
From the current directory run
```
python3 tcl_installer.py
```
You need to provide the hostname of the computer, IP, disk to format. It is interactif
## Post installation with Ansible
We will use Ansible-pull to download all the project we need on the computer and run it please be aware that the code need access to S3 enable the VPN
From the crontab we will download and install ansible roles
```
sudo su
crontab -e
```
Modify the line below as you need (note : update needed trop sale)
```
@reboot sleep 20 && ansible-pull -d /tmp/ansibleRole/ -U https://gitlab.epfl.ch/ecps/ansible-ecps/ --vault-password-file /root/.vault --full playbook.yml -i inventory/local --accept-host-key 2>&1 | tee /tmp/ansible-pull.txt

@reboot sleep 40 && ansible-galaxy install -r /tmp/ansibleRole/requirements.yml --roles-path /tmp/ansibleRole/roles 2>&1 | tee /tmp/ansible-galaxy.txt

@reboot sleep 60 && ansible-pull -d /tmp/ansibleRole/ -U https://gitlab.epfl.ch/ecps/ansible-ecps/ --vault-password-file /root/.vault --full playbook.yml -i inventory/local --accept-host-key 2>&1 | tee /tmp/ansible-playbooks.txt
```
you can find the logs in /tmp/ansible-pull.txt /tmp/ansible-galaxy.txt /tmp/ansible-playbooks

# Project made by [Thomas Christoph Müller](mailto:christoph.mueller@epfl.ch)
