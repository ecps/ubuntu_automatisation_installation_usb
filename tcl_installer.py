#!/usr/bin/python3

import os
import sys
import subprocess
import argparse
import socket
import uuid
import ipaddress
import shlex
import json
import math

from consolemenu import SelectionMenu

def is_efi():
	"""Returns true if the thumb drive has been booted through EFI"""
	return os.path.exists("/sys/firmware/efi")
	
def pdevice(device):
	"""handles the "p" prefix in front of partition numbers for nvme drives"""
	if "nvme" in device:
		return device+"p"
	return device

def bdevice(device):
	"""returns the basename of the device"""
	if os.path.exists(device):
		return os.path.basename(device)
	return device

def parted(device, scheme="efi", scratch = "scratch", part_layout_json=os.path.dirname(os.path.realpath(__file__))+"/image/partition_layout.json"):
	"Creates partiotion scheme based on sizes determined in part_layout_json"
	dev = os.path.basename(device)
	with open("/sys/block/{}/queue/optimal_io_size".format(dev),"r") as f:
		optimal_io_size = int(f.read())
	with open("/sys/block/{}/alignment_offset".format(dev),"r") as f:
		alignment_offset = int(f.read())
	with open("/sys/block/{}/queue/physical_block_size".format(dev),"r") as f:
		physical_block_size = int(f.read())
	with open("/sys/block/{}/queue/logical_block_size".format(dev),"r") as f:
		logical_block_size = int(f.read())
	optimal_sector_alignment = (optimal_io_size + alignment_offset)/physical_block_size
	if optimal_sector_alignment == 0:
		optimal_sector_alignment = 2048
	meg_to_sec = lambda x: x*1024*1024/logical_block_size
	
	with open(part_layout_json, "r") as f:
		part_layouts = json.load(f)
		label = {"efi": "gpt", "bios": "msdos"}
		layout = part_layouts[scheme][scratch]
		pt_start = optimal_sector_alignment
		command = "parted {} --script -- mklabel {}".format(device, label[scheme])
		for plabel, label, fstype, size in layout:
			pt_start_s = "{}s".format(int(pt_start))
			if size == None:
				pt_end_s = "100%"
			else:
				pt_end = pt_start + math.ceil((meg_to_sec(size)/optimal_sector_alignment))*optimal_sector_alignment - 1
				pt_end_s = "{}s".format(int(pt_end))
			command += " mkpart {} {} {} {}".format(plabel, fstype,pt_start_s,pt_end_s)
			pt_start = pt_end + 1
			if size == None:
				break
		if scheme=="bios":
			command += " set 1 boot on"
		else: 
			command += " set 1 esp on"
		return command

	
def lookup_host(hostname):
	"""looks up the ip if the hostname can be resolved"""
	try: 
		for i in socket.getaddrinfo(hostname,0):
			if i[0] is socket.AF_INET and i[1] is socket.SocketKind.SOCK_RAW: 
				return i[-1][0]
		return None
	except:
		return None

def install_image_efi(device, path=os.path.dirname(os.path.realpath(__file__))):
	"""Installs the system using the EFI boot scheme on a GPT partition table"""
	subprocess.run(["swapoff", "-a"])
	#print("\n--- Restoring GPT partition table ---")
	#subprocess.run(["sgdisk", "-g", "-l", path+"/image/partition_table.gpt", device])
	print("\n--- creating GPT partition table ---")
	scratch = find_scratch(blacklist=[bdevice(device)])
	if scratch != None:
		print(" ... Found scratch on {} ({})".format(scratch["NAME"],scratch["SIZE"]))
		subprocess.run(parted(device, "efi", "noscratch"), shell = True)
	else:
		print("\n ... Found no scratch, will create one at the end of the disk")
		subprocess.run(parted(device, "efi", "scratch"), shell = True)
	print("\n--- Restoring EFI Partition ---")
	subprocess.run(["partclone.fat32", "-r", "-s", path+"/image/p1_efi", "-o", "{}1".format(pdevice(device))])
	print("\n--- Restoring BOOT partition ---")
	subprocess.run(["partclone.xfs", "-r", "-s", path+"/image/p2_boot", "-o", "{}2".format(pdevice(device))])
	subprocess.run(["xfs_admin", "-L", "LINUX_BOOT", "{}2".format(pdevice(device))])
	print("\n--- Restoring ROOT partition ---")
	subprocess.run("cat "+path+"/image/p4_root.gz.part* | gunzip -c | partclone.xfs -r -o {}4".format(pdevice(device)), shell=True)
	subprocess.run(["xfs_admin", "-L", "LINUX_ROOT", "{}4".format(pdevice(device))])
	print("\n--- Creating SWAP ---")
	subprocess.run(["mkswap", "-U", "e7953bf4-390a-421d-8843-410762f4a733", "{}3".format(pdevice(device))])
	print("\n--- Looking for an existing scratch disk ---")
	scratch = find_scratch()
	if scratch == None:
		print("Nothing to scratch in sight... \nlets create a scratch partition on the remaining free space of the drive")
		subprocess.run(["sgdisk", "-N", device])
		print("reating file system ...")
		subprocess.run(["mkfs.xfs","-L", "SCRATCH", "{}5".format(pdevice(device))])

def post_installation_efi(device, config, path=os.path.dirname(os.path.realpath(__file__))):
	"""Post installation for EFI"""
	print("\n--- Mounting Target ---")
	subprocess.run(["mount", "{}4".format(pdevice(device)), path+"/target"])
	print("\n--- Growing XFS ---")	
	subprocess.run(["xfs_growfs", path+"/target"])
	post_installation_common(device, config, path)
	print("\n--- Unmounting Target ---")
	subprocess.run(["umount", path+"/target"])
	
def install_image_bios(device, path=os.path.dirname(os.path.realpath(__file__))):
	"""Installs the system using the BIOS boot scheme on a MBR partition table"""
	subprocess.run(["swapoff", "-a"])
	#subprocess.run("sfdisk {1} < {0}".format(path+"/image/partition_table.mbr", device), shell=True)
	print("\n--- Creating MBR partition table ---")
	scratch = find_scratch(blacklist=[bdevice(device)])
	if scratch != None:
		print(" ... Found scratch on {} ({})".format(scratch["NAME"],scratch["SIZE"]))
		subprocess.run(parted(device, "bios", "noscratch"), shell = True)
	else:
		print("\n ... Found no scratch, will create one at the end of the disk")
		subprocess.run(parted(device, "bios", "scratch"), shell = True)
	print("\n--- Restoring BOOT partition ---")
	subprocess.run(["partclone.xfs", "-r", "-s", path+"/image/p2_boot", "-o", "{}1".format(pdevice(device))])
	print("\n--- Restoring ROOT partition ---")
	subprocess.run("cat "+path+"/image/p4_root.gz.part* | gunzip -c | partclone.xfs -r -o {}3".format(pdevice(device)), shell=True)
	print("\n--- Creating SWAP ---")
	subprocess.run(["mkswap", "-U", "e7953bf4-390a-421d-8843-410762f4a733", "{}2".format(pdevice(device))])
	
def post_installation_bios(device, config, path=os.path.dirname(os.path.realpath(__file__))):
	"""Post installation for BIOS"""
	print("\n--- Mounting Target ---")
	subprocess.run(["mount", "{}3".format(pdevice(device)), path+"/target"])
	subprocess.run(["mount", "{}1".format(pdevice(device)), path+"/target/boot"])
	subprocess.run("mount -o bind /dev {}/dev".format(path+"/target"), shell=True)
	subprocess.run("mount -o bind /sys {}/sys".format(path+"/target"), shell=True)
	subprocess.run("mount -o bind /proc {}/proc".format(path+"/target"), shell=True)
	print("\n--- Growing XFS ---")	
	subprocess.run(["xfs_growfs", path+"/target"])
	subprocess.run(["xfs_growfs", path+"/target/boot/"])
	print("\n--- Installing GRUB ---")
	print("\n   ... chroot into target ({})".format(path+"/target"))
	ACTUAL_ROOT = os.open("/", os.O_PATH)
	os.chroot(path+"/target")
	os.chdir("/")
	print("\n   ... removing /boot/grub/grubenv")
	os.remove("/boot/grub/grubenv")
	print("\n   ... running grub-mkconfig")
	subprocess.run("grub-mkconfig > /boot/grub/grub.cfg", shell=True)
	print("\n   ... running grub-install on {}".format(device))
	subprocess.run("""grub-install --target=i386-pc {}""".format(device), shell=True)
	subprocess.run("sed -i 's/linuxefi /linux /g' /boot/grub/grub.cfg", shell=True)
	subprocess.run("sed -i 's/initrdefi /initrd /g' /boot/grub/grub.cfg", shell=True)
	os.chdir(ACTUAL_ROOT)
	os.chroot(".")
	os.chdir(path)

	post_installation_common(device, config, path)
	print("\n--- Removing EFI partition from fstab ---")
	lines=[]
	with open("target/etc/fstab", "r") as f:
		lines = f.readlines()
	with open("target/etc/fstab", "w") as f:
		for line in lines:
			if not "/boot/efi" in line:
				f.write(line)
	print("\n--- Unmounting Target ---")
	subprocess.run("umount {}/dev".format(path+"/target"), shell=True)
	subprocess.run("umount {}/sys".format(path+"/target"), shell=True)
	subprocess.run("umount {}/proc".format(path+"/target"), shell=True)
	subprocess.run("umount {}/boot".format(path+"/target"), shell=True)
	subprocess.run("umount {}".format(path+"/target"), shell=True)

def find_scratch(scratchlabels=["SCRATCH", "/scratch"], blacklist=[]):
	disk_info = enumerate_disks()
	for k,v in disk_info["PARTITIONS"].items():
		if v["LABEL"] in scratchlabels and v["PKNAME"] not in blacklist:
			return v
	return None

ansible_keys = 	"""
#Pull
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCnTxc5CLW3Fqhff0QkZhTeHYZQXPH02Md08gpYEqlwLTIh6ecOJGSjSzSI/tZYhB3LT60wye8CsYQucim6nhS3lMv0Z7Uo6nIA5GnH2sW54WeJ6brY5/tivBeVGEuCAY7J6IHI6ZA2/eBBTweFde1AdUSzLz3gXvWguOXya8DSZF+iKXXM3Wz/TiNbW9Du/zcQXcvRtVeMOwSSQNNwMoF3UTHXJDbmDnFrWCAqDrWWahmYTWO0Lgvos49/4HogYuzvLm9+ufQIv6+VA73HIDrzhgAWrUW2mUQC2WedrKjLKIl4lAtMy83X1DY71NIgX4xqHDhgBDZZXfofQa6GOg999YzBFnH1hi1xaXeswy9x2a84Z7AvfY9qnOXJ2/Re5+ChmZF4MwJQf/c+dd772UDI7+C1UYOL/ah4jMb5cU217S/KfH5yuZeDlM+BmCi5fQj1klxgSHTZq4FziX2up+isycT6ZBNpJhk80hVqBUojh38XhI3YOeLMiuQaCWHMgH+wdidF1TzTEel8b+W0Zh4HZJ/GnIV1PnoUdYbwBv446Ed5CWBQ1GOdzh3GcVfyKs0o6DF3Cp27qPHsu248SvbWNQxbTVrYGXj6moUcwtkP9f4uKtLBabo1tv1OhBJphU9YTkTfaHCMl8fmEkWq+8YwtFo3Osf2yCViz7ETWUAbWw== pullyvan.krishnamoorthy@epfl.ch
#Jenkins
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBwiMftRitwBcSvlK6CFbcHZRi/isEksmHRjIqfpIMUHI7rzkIT3IH07q9QyyPxKBfw2JkcNljvdBMS6hdeny4dLUeb/kNHPPHZ9Xb/egw7FvLHTYc+i+rzZLQ4NmIL+qTmNrBBA/Lj+gVhidu7Rt2y/5o/7HFDMz9i3lLJkS+zgqULrJN+nptf6kVY9slryHYuLuJgmcTM+I0VcSm1NAYxxAAkIUFRv3ydQgB1j0FhQk89I/ovlBbblTBxN50vTYmiWSC9FlSEqRZqF/48wCGtn0SMmjuea/lqy9BDOlRGITHS9phlfULXms9A62INCNjBEBDesd0qV7Fo2fYFogb jaep@tsf-476-wpa-6-095.epfl.ch
#Manu
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCw9mzsm1PICS8SHO6rPriyPvdBme2a7bcAvgUcmh+btb1oc9TGHeZ9jnX4WK+D8qlanjbGQfK1nRxj9zzva6tFi7MM/zYGQh64w1Vs89ci8inyP/L1LzGlw01GoJjNDJ/ww5/HE+xu/fer/keHr91fR0jVdBaNMhu2O48cvVXr85eC6YLYfvxTv0bSalSkf5vNgLuYrTukCFq6ummk9EzVHwvvuGnBoVpSczj3b1wr0UM00ePxsRHBWuphnEI1BTsfLaGlFo3+sD3sy7PVKVQfRpBmz0PID5MmqxJpRnxqf6xbHf6HMAtJw1KlMH4d0DLjMQyP23dxtk9KeLA00gx1 jenkins@63bcf9ed3008
"""
def post_installation_common(device, config, path):
	"""Common post installation tasks which are identical for BIOS and EFI"""
	print("\n--- Setting Target Hostname to {} ---".format(config["hostname"]))
	with open(path+"/target/etc/hostname", "w") as f:
		f.write(config["hostname"])
	print("\n--- Configuring Network (IP={})---".format(config["ip"]))
	network_script = ""
	with open(path+"/templates/01-network-manager-all.yaml", "r") as f:
		network_script = f.read().format(str(uuid.uuid4()), config["ip"], config["gw"])	
	with open(path+"/target/etc/netplan/01-network-manager-all.yaml", "w") as f:
		f.write(network_script)
	print("\n--- Remove SSH host keys---")
	subprocess.run(["rm", "-rf", path+"/target/etc/ssh/ssh_host_*_key.pub"])
	subprocess.run(["rm", "-rf", path+"/target/etc/ssh/ssh_host_*_key"])
	print("\n--- Adding authorized keys ---")
	with open(path+"/target/root/.ssh/authorized_keys","w") as f:
		f.write(ansible_keys)
	subprocess.run("chmod 700 " + path +"/target/root/.ssh/authorized_keys", shell=True)
	print("\n--- Identifying /scratch ---")
	v = find_scratch()
	if v != None:
		print("found on {} with fs type {}".format(v["NAME"], v["FSTYPE"]))
		with open(path + "/target/etc/fstab", "a") as f:
			f.write("LABEL={0}	/scratch	{1}	defaults	0	0".format(v["LABEL"], v["FSTYPE"]))


def lsblk(keys='NAME,KNAME,PKNAME,MODEL,SIZE,SERIAL,TYPE,FSTYPE,LABEL,MOUNTPOINT,PHY-SEC'):
	# gathers disk information from the system
	lsblk_output = subprocess.check_output(['lsblk', '-o', keys, '-P']).decode(sys.stdout.encoding).split('\n')
	disc_dicts = [dict([i.split("=") for i in shlex.split(a)]) for a in lsblk_output[:-1]]
	return disc_dicts

def enumerate_disks():
	# gathers disk information and preprocesses the data
	disc_dicts = lsblk()
	disk_info = {}
	disk_info["DISKS"] = {}
	disk_info["USED_DISKS"] = []
	disk_info["PARTITIONS"] = {}
	for i in disc_dicts:
		if i["TYPE"]=="disk":
			disk_info["DISKS"][i["NAME"]] = i
			disk_info["DISKS"][i["NAME"]]["PARTITIONS"]=[]
			disk_info["DISKS"][i["NAME"]]["PARTITION_COUNT"]=0
		if i["TYPE"]=="part":
			disk_info["DISKS"][i["PKNAME"]]["PARTITION_COUNT"]+=1
			disk_info["DISKS"][i["PKNAME"]]["PARTITIONS"].append(i)
			disk_info["PARTITIONS"][i["NAME"]]=i
			if i["PKNAME"] not in disk_info["USED_DISKS"]  and len(i["MOUNTPOINT"]) and i["MOUNTPOINT"]!="[SWAP]":
				disk_info["USED_DISKS"].append(i["PKNAME"]) 
	return disk_info
def menu():
	disk_info = enumerate_disks()
	(disks, used_disks) = (disk_info["DISKS"], disk_info["USED_DISKS"]) 
	valid_disks = [i for i in disks if i not in used_disks]
	print(valid_disks)
	# we only allow disks that have no partition mounted
	labels = ["{}, {}, {} Partition(s)".format(disks[i]["NAME"], disks[i]["SIZE"], disks[i]["PARTITION_COUNT"]) for i in valid_disks]
	system = "a BIOS system"
	if is_efi():
		system = "an EFI system"	
	disk_selection = SelectionMenu.get_selection(labels, title="Welcome to the TCL system restore tool running on {}.".format(system),subtitle="Please select disk to restore image to:")
	try:
		device_name = valid_disks[disk_selection]
		device="/dev/" + device_name
	except:
		exit()

	hostname=input("enter hostname: ")
	ip = lookup_host(hostname)
	if ip == None:
		ip=input("enter ip: ")
	try:
		ipaddress.IPv4Address(ip)
	except ipaddress.AddressValueError:
		print("Invalid IP: {}".format(ip))
		exit()

	gw = ".".join(ip.split(".")[0:3])+".1"
	print("Machine configuration: {} with IP {} / GW {}".format(hostname, ip, gw))
	
	scratch=find_scratch(blacklist = [device_name])

	config = {
		"ip" : ip,
		"hostname": hostname,
		"gw": gw,
		"scratch": scratch
	}
	
	print("WARNING: all data will be deleted on {}!".format(device))
	devicename=input("enter device name again to continue: ")
	if device != devicename:
		exit()
	
	if is_efi():
		install_image_efi(device)
		post_installation_efi(device,config)
	else:
		install_image_bios(device)
		post_installation_bios(device,config)

if __name__ == "__main__":
	menu()

